package com.company.decorators;

public interface DataSource {

    void writeData(String data);

    String readData();

}
