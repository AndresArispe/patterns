package com.company;

import com.company.EnemyShip;
import com.company.EnemyShipFactory;

public class UFOEnemyShip extends EnemyShip {

    EnemyShipFactory shipFactory;
    public UFOEnemyShip(EnemyShipFactory shipFactory){
        this.shipFactory = shipFactory;
    }

    public void makeShip() {
        System.out.println("Making enemy ship " + getName());
        weapon = shipFactory.addESGun();
        engine = shipFactory.addESEngine();
    }

}
