package com.company.factory;

import com.company.ships.BigUFOEnemyShip;
import com.company.ships.EnemyShip;
import com.company.ships.RocketEnemyShip;
import com.company.ships.UFOEnemyShip;

public class EnemyShipFactory {

    public EnemyShip makeEnemyShip(String newShipType) {
        EnemyShip enemyShip = null;

        if (newShipType.equals("U")) {
            return new UFOEnemyShip();
        }
        else {
            if (newShipType.equals("R")) {
                return new RocketEnemyShip();
            }
            else {
                if (newShipType.equals("U")) {
                    return new BigUFOEnemyShip();
                }
            }
        }
        return enemyShip;
    }
}
