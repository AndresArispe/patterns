package com.company.ships;

import com.company.ships.EnemyShip;

public class RocketEnemyShip extends EnemyShip {

    public RocketEnemyShip() {
        setName("Rocket Enemy Ship");
        setAmtDamage(10.0);
    }
}
