package com.company.ships;

import com.company.ships.EnemyShip;

public class UFOEnemyShip extends EnemyShip {

    public UFOEnemyShip() {
        setName("UFO Enemy Ship");
        setAmtDamage(20.0);
    }

}
