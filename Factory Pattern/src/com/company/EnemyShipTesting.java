package com.company;

import com.company.factory.EnemyShipFactory;
import com.company.ships.EnemyShip;
import com.company.ships.RocketEnemyShip;
import com.company.ships.UFOEnemyShip;

import java.util.Scanner;

public class EnemyShipTesting {

    public static void main(String[] args) {

        EnemyShipFactory enemyShipFactory = new EnemyShipFactory();

        EnemyShip enemyShip = null;

        Scanner userInput = new Scanner(System.in);

        System.out.println("What type of ship? U/R/B");

        if (userInput.hasNextLine()) {
            String typeOfShip = userInput.nextLine();

            enemyShip = enemyShipFactory.makeEnemyShip(typeOfShip);
        }
        if (enemyShip != null) {
            doStuffEnemy(enemyShip);
        }
        else {
            System.out.println("Wrong letter...");
        }

    }

    private static void doStuffEnemy(EnemyShip ufoShip) {
        ufoShip.displayEnemyShip();
        ufoShip.followHeroShip();
        ufoShip.enemyShipShoots();
    }


}
